import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import './style.scss';

const Spinner = () => (
  <div className="spinner">
    <LinearProgress variant="query" />
  </div>
);

export default Spinner;
