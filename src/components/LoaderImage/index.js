import React, {Component} from 'react';
import Spinner from 'components/Spinner';
import PropTypes from 'prop-types';

class LoaderImage extends Component {
  state = {
    isLoadImg: false,
  };
  static propTypes = {
    src: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    style: PropTypes.object.isRequired,
  };
  loadImage = () => {
    this.setState ({isLoadImg: true});
  };
  componentDidMount () {
    const {src} = this.props;
    const img = new Image ();
    img.src = src;
    img.addEventListener ('load', this.loadImage);
  }
  render () {
    const {children, style} = this.props;
    const {isLoadImg} = this.state;
    if (isLoadImg) return children;
    return <div style={style}><Spinner /></div>;
  }
}

export default LoaderImage;
