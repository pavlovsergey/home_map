import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Error = ({error: {message}}) => {
  const labelText = message ? `${message}` : 'Sorry an error has occurred.';

  return (
    <div className="error__container">
      {labelText}
    </div>
  );
};

Error.propTypes = {
  error: PropTypes.object,
};

export default Error;
