import React, {Component} from 'react';
import HomeCard from './HomeCard';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import './style.scss';

class CardList extends Component {
  static propTypes = {
    cards: PropTypes.array.isRequired,
    cardTemplate: PropTypes.array,
    cardTemplates: PropTypes.array.isRequired,
  };

  getBody = () => {
    const {cards, cardTemplate, handleClickCard, cardTemplates} = this.props;

    return cards.map ((card, index) => (
      <Grid key={card.id} item xs={12} sm={12} md={6} lg={4} xl={3}>
        <div className="card_container">
          <HomeCard
            key={card.id}
            id={index}
            card={card}
            cardTemplate={cardTemplate}
            handleClickCard={handleClickCard}
            cardTemplates={cardTemplates}
          />
        </div>
      </Grid>
    ));
  };

  render () {
    return (
      <Grid container spacing={32}>
        {this.getBody ()}
      </Grid>
    );
  }
}

export default CardList;
