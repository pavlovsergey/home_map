import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Address = ({address}) => (
  <p className="address">
    {address}
  </p>
);

Address.propTypes = {
  address: PropTypes.string.isRequired,
};

export default Address;
