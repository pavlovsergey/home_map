import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Area = ({area}) => (
  <p className="area">
    {`${area} sq. ft.`}
  </p>
);

Area.propTypes = {
  area: PropTypes.number.isRequired,
};

export default Area;
