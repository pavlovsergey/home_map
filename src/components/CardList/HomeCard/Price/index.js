import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Price = ({price}) => {
  const transformPrice = `$ ${price.match (/.{1,3}/g).join (' ')}`;
  return (
    <p className="price">
      <span className="label">{transformPrice}</span>
    </p>
  );
};

Price.propTypes = {
  price: PropTypes.string.isRequired,
};

export default Price;
