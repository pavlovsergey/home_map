import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardActionArea from '@material-ui/core/CardActionArea';
import Address from './Address';
import Price from './Price';
import Area from './Area';
import CardImage from './CardImage';
import IconButton from '@material-ui/core/IconButton';
import ShareIcon from '@material-ui/icons/Share';
import SettingsCardModal from './SettingsCardModal';
import './style.scss';

class HomeCard extends React.Component {
  static propTypes = {
    card: PropTypes.object.isRequired,
    isSettings: PropTypes.bool,
    cardTemplates: PropTypes.array,
    cardTemplate: PropTypes.array.isRequired,
  };

  constructor (props) {
    super (props);
    this.cardMap = {
      full_address: this.getAddressComponent,
      area: this.getAreaComponent,
      price: this.getPriceComponent,
      images: this.getImageComponent,
    };

    this.state = {
      isOpenSettings: false,
      template: props.cardTemplate,
    };

    this.styleChildren = {marginTop: '-60px'};
  }

  handleCloseSettings = () => {
    this.setState ({isOpenSettings: false});
  };

  handleClickImage = () => {
    const {id, handleClickCard} = this.props;
    handleClickCard && handleClickCard (id);
  };

  handleOpenSettings = () => {
    this.setState ({isOpenSettings: true});
  };
  getAddressComponent = () => {
    const {card: {full_address: address}} = this.props;
    return <Address address={address} />;
  };
  getPriceComponent = () => {
    const {card: {price}} = this.props;
    return <Price price={price.toString ()} />;
  };

  getAreaComponent = () => {
    const {card: {area}} = this.props;
    return area ? <Area area={area} /> : null;
  };

  getImageComponent = children => {
    const {card: {images}} = this.props;
    const srcImages = images.lenght === 0 ? ['./defaultHomeImg.jpg'] : images;

    if (children) {
      return (
        <CardActionArea onClick={this.handleClickImage}>
          {this.getBody (children)}
          <CardImage style={this.styleChildren} images={srcImages} />
        </CardActionArea>
      );
    }

    return (
      <CardActionArea onClick={this.handleClickImage}>
        <CardImage images={srcImages} />
      </CardActionArea>
    );
  };

  handleChooseTemplate = id => {
    const {cardTemplates} = this.props;
    const template = cardTemplates[id].template;
    this.setState ({
      template,
      isOpenSettings: false,
    });
  };

  getBody = childrenTemplate => {
    const {template} = this.state;
    const templat = template;
    const arr = childrenTemplate ? childrenTemplate : templat;

    return arr.map (({field, children}, index) => {
      return (
        <React.Fragment key={index}>
          {this.cardMap[field] (children)}
        </React.Fragment>
      );
    });
  };

  render () {
    const {isOpenSettings} = this.state;
    const {isSettings} = this.props;

    return (
      <Card className="card">
        {this.getBody ()}
        {!isSettings &&
          <CardActions className="actions" disableActionSpacing>
            <IconButton aria-label="Share" onClick={this.handleOpenSettings}>
              <ShareIcon />
            </IconButton>
          </CardActions>}
        {isOpenSettings &&
          <SettingsCardModal
            open={isOpenSettings}
            onClose={this.handleCloseSettings}
            handleChooseTemplate={this.handleChooseTemplate}
          />}
      </Card>
    );
  }
}

export default HomeCard;
