import React from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import HomeCard from '../';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {connect} from 'react-redux';
import './style.scss';

const cardDefault = {
  id: 1,
  full_address: 'full_address',
  description: 'description',
  images: ['./defaultHomeImg.jpg'],
  area: 100,
  price: 1000000,
};

class SettingsCardModal extends React.Component {
  static propTypes = {
    onClose: PropTypes.func.isRequired,
    handleChooseTemplate: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };
  getSettingsBody = () => {
    const {cardTemplates, handleChooseTemplate} = this.props;
    return cardTemplates.map (({id, template}, index) => (
      <Grid key={id} item xs={12} sm={12} md={5} lg={3} xl={3}>
        <div className="card_container">
          <HomeCard
            key={id}
            id={index}
            card={cardDefault}
            cardTemplate={template}
            handleClickCard={handleChooseTemplate}
            isSettings={true}
          />
        </div>
      </Grid>
    ));
  };

  render () {
    const {open, onClose} = this.props;
    return (
      <Dialog
        onClose={onClose}
        aria-labelledby="simple-dialog-title"
        open={open}
        maxWidth="xl"
        scroll="body"
      >
        <DialogTitle id="simple-dialog-title" onClose={onClose}>
          Сlick on a image to select a template.
          <IconButton
            aria-label="Close"
            className="modal__iconClose"
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <div className="modal__container">
          <Grid container justify="center" spacing={32}>
            {this.getSettingsBody ()}
          </Grid>
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    cardTemplates: state.cardTemplates.data,
  };
};

export default connect (mapStateToProps) (SettingsCardModal);
