import React from 'react';
import PropTypes from 'prop-types';
import LoaderImage from 'components/LoaderImage';
import CardMedia from '@material-ui/core/CardMedia';
import './style.scss';
const styles = {
  height: 0,
  paddingTop: '56.25%',
};
const CardImage = ({images, style}) => (
  <LoaderImage src={images[0]} style={styles}>
    <CardMedia
      className="card__img"
      style={style}
      image={images[0]}
      title="card image"
    />
  </LoaderImage>
);

CardImage.propTypes = {
  images: PropTypes.array.isRequired,
};

export default CardImage;
