import React from 'react';
import Spinner from 'components/Spinner';
import './style.scss';

const SpinnerContainer = () => (
  <div className="spinner__container">
    <Spinner />
  </div>
);

export default SpinnerContainer;
