import {LOAD_CARDS} from 'store/constants/card';

const stateCardDefault = [];

const card = (stateCard = stateCardDefault, {type, payload}) => {
  switch (type) {
    case LOAD_CARDS:
      return payload.data;

    default:
      return stateCard;
  }
};

export default card;
