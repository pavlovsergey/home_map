import {
  LOADING_CARDS,
  LOAD_CARDS_FAIL,
  LOADING_TEMPLATE_CARD,
  LOAD_TEMPLATE_CARD_FAIL,
} from 'store/constants/communication';
import {LOAD_CARDS} from 'store/constants/card';
import {LOAD_TEMPLATE_CARD} from 'store/constants/templateCard';

import {setLoadingTrue, setLoadTrue, setLoadFail} from 'utils/communication';

const defaultState = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: {message: '', name: ''},
};

const stateCommunicationDefault = {
  cards: defaultState,
  templateCard: defaultState,
};

const communication = (
  stateCommunication = stateCommunicationDefault,
  {type, payload}
) => {
  switch (type) {
    case LOADING_CARDS:
      return {
        ...stateCommunication,
        cards: setLoadingTrue (stateCommunication.cards),
      };

    case LOAD_CARDS:
      return {
        ...stateCommunication,
        cards: setLoadTrue (stateCommunication.cards),
      };

    case LOAD_CARDS_FAIL:
      return {
        ...stateCommunication,
        cards: setLoadFail (stateCommunication.cards, payload),
      };

    case LOADING_TEMPLATE_CARD:
      return {
        ...stateCommunication,
        templateCard: setLoadingTrue (stateCommunication.templateCard),
      };

    case LOAD_TEMPLATE_CARD:
      return {
        ...stateCommunication,
        templateCard: setLoadTrue (stateCommunication.templateCard),
      };

    case LOAD_TEMPLATE_CARD_FAIL:
      return {
        ...stateCommunication,
        templateCard: setLoadFail (stateCommunication.templateCard, payload),
      };

    default:
      return stateCommunication;
  }
};

export default communication;
