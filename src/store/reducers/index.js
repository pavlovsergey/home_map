import {combineReducers} from 'redux';
import card from './card';
import communication from './communication';
import cardTemplates from './templateCard';

export default combineReducers ({card, communication, cardTemplates});
