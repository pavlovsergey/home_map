import {LOAD_TEMPLATE_CARD} from 'store/constants/templateCard';

const cardTemplatesDefault = {
  data: [],
  choseCardTemplate: 0,
};

const cardTemplates = (
  stateCardTemplates = cardTemplatesDefault,
  {type, payload}
) => {
  switch (type) {
    case LOAD_TEMPLATE_CARD:
      return {
        ...stateCardTemplates,
        data: payload.data,
      };

    default:
      return stateCardTemplates;
  }
};

export default cardTemplates;
