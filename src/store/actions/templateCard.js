import {LOAD_TEMPLATE_CARD} from 'store/constants/templateCard';
import {
  LOADING_TEMPLATE_CARD,
  LOAD_TEMPLATE_CARD_FAIL,
} from 'store/constants/communication';
import {handlerError} from './error';
import {polyfill} from 'es6-promise';
import axios from 'axios';
polyfill ();
axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadCardTemplates = () => {
  return async dispatch => {
    try {
      dispatch ({type: LOADING_TEMPLATE_CARD});

      const {data} = await axios.get (
        'http://demo4452328.mockable.io/templates'
      );
      return dispatch ({
        type: LOAD_TEMPLATE_CARD,
        payload: {data},
      });
    } catch (e) {
      const message = 'Sorry, there was an error loading template card';
      dispatch (handlerError (LOAD_TEMPLATE_CARD_FAIL, message, e));
    }
  };
};
