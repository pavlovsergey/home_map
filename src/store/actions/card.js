import {LOAD_CARDS} from 'store/constants/card';
import {LOADING_CARDS, LOAD_CARDS_FAIL} from 'store/constants/communication';
import {handlerError} from './error';
import {polyfill} from 'es6-promise';

import axios from 'axios';
polyfill ();
axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadHomeCards = () => {
  return async dispatch => {
    try {
      dispatch ({type: LOADING_CARDS});

      const {data: {data}} = await axios.get (
        'http://demo4452328.mockable.io/properties'
      );

      return dispatch ({
        type: LOAD_CARDS,
        payload: {data},
      });
    } catch (e) {
      const message = 'Sorry, there was an error loading cards';
      dispatch (handlerError (LOAD_CARDS_FAIL, message, e));
    }
  };
};
