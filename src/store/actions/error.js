export const handlerError = (type, message, e) => {
  return {
    type,
    payload: {
      message: `${message}. ${e.message}`,
    },
  };
};
