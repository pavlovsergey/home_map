import React from 'react';
import Routes from 'routes';
import './style.scss';

const Layout = () => (
  <div className="layout_container">
    <Routes />
  </div>
);

export default Layout;
