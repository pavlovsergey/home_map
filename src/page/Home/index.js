import React, {Component} from 'react';
import Loader from 'components/Loader';
import CardList from 'components/CardList';
import {connect} from 'react-redux';
import {loadHomeCards} from 'store/actions/card';
import {loadCardTemplates} from 'store/actions/templateCard';
import PropTypes from 'prop-types';

class Home extends Component {
  static propTypes = {
    comunicationCards: PropTypes.object.isRequired,
    comunicationTemplateCard: PropTypes.object.isRequired,
    loadHomeCards: PropTypes.func.isRequired,
    loadCardTemplates: PropTypes.func.isRequired,
    cardTemplates: PropTypes.array.isRequired,
    cards: PropTypes.array.isRequired,
    choseCardTemplate: PropTypes.number.isRequired,
  };

  state = {
    chooseCard: 0,
  };
  componentDidMount () {
    const {
      comunicationCards: {isLoading, isLoad, isErrorLoad},
      loadHomeCards,
      comunicationTemplateCard: {
        isLoading: tmpIsLoading,
        isLoad: tmpIsLoad,
        isErrorLoad: tmpIsErrorLoad,
      },
      loadCardTemplates,
    } = this.props;

    if (!isLoading && !isLoad && !isErrorLoad) loadHomeCards ();
    if (!tmpIsLoading && !tmpIsLoad && !tmpIsErrorLoad) loadCardTemplates ();
  }

  render () {
    const {
      comunicationCards,
      comunicationTemplateCard,
      cards,
      cardTemplates,
      choseCardTemplate,
    } = this.props;

    return (
      <Loader comunication={[comunicationCards, comunicationTemplateCard]}>
        <div className="cardList_container">
          {cardTemplates[choseCardTemplate] &&
            cards.length &&
            <CardList
              cards={cards}
              cardTemplate={cardTemplates[choseCardTemplate].template}
              cardTemplates={cardTemplates}
            />}
        </div>
      </Loader>
    );
  }
}

const mapStateToProps = state => {
  return {
    comunicationCards: state.communication.cards,
    comunicationTemplateCard: state.communication.templateCard,
    cards: state.card,
    cardTemplates: state.cardTemplates.data,
    choseCardTemplate: state.cardTemplates.choseCardTemplate,
  };
};
const mapDispatchToProps = {loadHomeCards, loadCardTemplates};
export default connect (mapStateToProps, mapDispatchToProps) (Home);
